import thread
from flask import Flask,jsonify
from pattern.en import tag
from SentievalTwitter import SentievalTwitter
from TwitterHybridClassifier import TwitterHybridClassifier
from topic_extractor import NPExtractor




		
app = Flask(__name__)
mutex = thread.allocate_lock()

train_path='Data/training_neatfile.csv'
dev_path='Data/output_file_train.tsv'
test_path='Data/sms-test-gold-B.tsv'

sentieval = SentievalTwitter(train_path,dev_path,test_path)
trainset = sentieval.trainset


# Training the supervised model
print "Training..."
classifier = TwitterHybridClassifier(trainset)
print "training done..."

# Apply the classifier for all tweets in the testset
##output_file = 'test_set_prediction.output'
##fp = open(output_file,'w')
##for num,tweet in enumerate(testset):
##    print "Processing...",num
##    classifier_type,score,tweet_class = classifier.classify(tweet['MESSAGE'])
##    print tweet_class
##    line = tweet['SID'] + '\t' + tweet['UID'] + '\t' + tweet_class + '\t' + tweet['MESSAGE']
##    fp.write(line)
##fp.close()

###Test on a set on unseen tweets
testTweet=open('sampleTweets.csv','rb')

for item in testTweet:
        np_extractor = NPExtractor(item)
        result = np_extractor.extract()
        print result
        classifier_type,score,sentiment=classifier.classify(item)     
        print item,"\n",classifier_type,score,sentiment
        print ("\n");
		
		
@app.route('/')
def main_handler():
   return "hello!!"
   
@app.route('/sentiment/api/v1.0/tasks/<text>', methods = ['GET'])
def sentiment_analysis(text):
        mutex.acquire()
        np_extractor = NPExtractor(text)
        result = np_extractor.extract()
               
        classifier_type,score,sentiment=classifier.classify(text)
        if score==0:
                sentiment=='neutral'
        
        if classifier_type=='rb':
                score=score/10
        if classifier_type=='lb':
                score=score/10
                   
        
        mutex.release()        
        return jsonify({
                        'sentiments':{'score':score,'type':sentiment},
                        'topic_keywords':[{'relevance':0.5,'text':word+","} for word in result],
                        
                        })

	
if __name__ == '__main__':
    app.run(port=8888,threaded=True)	#insert host='0.0.0.0' to make the code listen to public ip addresses


These python scripts provide the Hybrid approach classifier.

Running python run_classifier.py is going to generate a file 'test_set_prediction.output' which contains the predictions on the test set.

For using the hybrid classifier using a trainset of your own(the code uses a trainset in csv format,so use a CSV file).If any other file is used you need to modify the code.
The test set file is in tsv format.Its in the following format---
		<SID> <UID> <CLASS i.e pos/neg/neutral> <Tweet/text>

For using the classifer in your own code you need to do the following----

    from TwitterHybridClassifier import TwitterHybridClassifier
    # you must build a trainset. See the SemevalTwitter.py to check the trainset format
    classifier = TwitterHybridClassifier(trainset)
    prediction = classifier.classify(tweet_message)
	
If you need to download a new training set or test set,you can use the download_tweets.py file.Use the following command

$ python downloads_tweets.py input_file>output_file	

in the input_file use-"twitter-train.tsv" file.
	
A training set "training_neatfile" is provided in the data folder.
To test,run the file "run_classifier.py"...it will listen for requests on port 8888.
To send a request type--"http://127.0.0.1:8888/sentiment/api/v1.0/tasks/<the test tweet or text>" in a browser or curl.
The run_classifier.py module performs a test on few unseen tweets in the file "sampleTweets.csv"


